package lib

import (
	"time"

	"github.com/jellydator/ttlcache/v3"
)

var singleton *SimpleCache[string, any]

type Cache[K comparable, V any] interface {
	Get(key K) *V
	Set(key K, value V, ttl time.Duration)
	Delete(key K)
	Flush()
}

type SimpleCache[K string, V any] struct {
	ttlCache *ttlcache.Cache[K, V]
}

func (sc *SimpleCache[K, V]) Get(key K) *V {
	item := sc.ttlCache.Get(key, ttlcache.WithDisableTouchOnHit[K, V]())

	if item == nil {
		return nil
	}

	value := item.Value()

	return &value
}

func (sc *SimpleCache[K, V]) Set(key K, value V, ttl time.Duration) {
	sc.ttlCache.Set(key, value, ttl)
}

func (sc *SimpleCache[K, V]) Delete(key K) {
	sc.ttlCache.Delete(key)
}

func (sc *SimpleCache[K, V]) Flush() {
	sc.ttlCache.DeleteAll()
}

func New() *SimpleCache[string, any] {
	if singleton == nil {
		singleton = &SimpleCache[string, any]{
			ttlCache: ttlcache.New[string, any](),
		}
	}

	return singleton
}
