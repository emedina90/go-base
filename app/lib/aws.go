package lib

import "github.com/aws/aws-sdk-go/aws/session"

var sess *session.Session

func AwsSession() *session.Session {
	if sess == nil {
		sess, _ = session.NewSession()
	}

	return sess
}
