package lib

import (
	"github.com/aws/aws-sdk-go/service/sns"
)

type Message struct {
	Body string
}

type INotificationService interface {
	Send(message Message) error
}

type NotificationService struct {
	topic string
	sns   *sns.SNS
}

func (ns NotificationService) Send(message Message) error {
	_, err := ns.sns.Publish(&sns.PublishInput{
		Message:  &message.Body,
		TopicArn: &ns.topic,
	})

	if err != nil {
		return err
	}

	return nil
}

func NewNotificationService(topic string) *NotificationService {
	svc := sns.New(AwsSession())

	return &NotificationService{topic, svc}
}
