package app

import (
	"github.com/gin-gonic/gin"
	"github.com/go-co-op/gocron"
)

type Module interface {
	ServeRoutes(server *gin.Engine)
	ScheduleTasks(scheduler *gocron.Scheduler)
}
