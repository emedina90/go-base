package base

import (
	"log"

	"github.com/go-co-op/gocron"
)

func schedules(scheduler *gocron.Scheduler) {
	scheduler.Every(1).Minute().Do(func() {
		log.Println("Schedule tasks here...")
	})
}
