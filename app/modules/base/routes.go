package base

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func routes(server *gin.Engine) {
	server.GET("/health", func(context *gin.Context) {
		context.JSON(http.StatusOK, 1)
	})
}
