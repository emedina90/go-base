package base

import (
	"github.com/gin-gonic/gin"
	"github.com/go-co-op/gocron"
)

type Module struct{}

func (this Module) ServeRoutes(server *gin.Engine) {
	routes(server)
}

func (this Module) ScheduleTasks(scheduler *gocron.Scheduler) {
	schedules(scheduler)
}
