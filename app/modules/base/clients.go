package base

import (
	"github.com/go-resty/resty/v2"
)

type httpClient struct {
	client  *resty.Client
	baseUrl string
}
