package app

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-co-op/gocron"
)

type Application struct {
	server    *gin.Engine
	scheduler *gocron.Scheduler
}

func NewApplication() *Application {
	return &Application{
		server:    gin.Default(),
		scheduler: gocron.NewScheduler(time.UTC),
	}
}

func (app *Application) Start() {
	app.server.GET("/ping", func(context *gin.Context) {
		context.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	app.scheduler.StartAsync()
	err := app.server.Run()
	if err != nil {
		panic("Unable to start server!")
	}
}

func (app *Application) RegisterModules(modules ...Module) {
	for _, module := range modules {
		module.ServeRoutes(app.server)
		module.ScheduleTasks(app.scheduler)
	}
}
