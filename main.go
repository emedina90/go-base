package main

import (
	"emedina90/go-base/app"
	"emedina90/go-base/app/modules/base"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()

	if err != nil {
		panic("Unable to load ENV file!")
	}

	application := app.NewApplication()

	// Register Modules heres
	application.RegisterModules(base.Module{})

	application.Start()
}
